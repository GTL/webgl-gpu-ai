var gl;
var models;
var gpu = false;
var influences;
var influenceFrameBuffer;
var influenceTexture;
var influenceMap;
var cpuInfluence = {};
var timer = 0;
var frequency = 20;
var influenceWidth = 128;
var influenceHeight = 128;
var numToInstance = 0;
var data = [];
var collect = false;

GPUAIWebGLInit = function(){
    
    var canvas = os.resschmgr.Create.HTMLElement("canvas");
    canvas.html().style.width = "100%";
    canvas.html().style.height = "100%";
    
    document.body.appendChild(canvas.html());
    document.body.style.overflow = "hidden";
    
    os.graphics.Load(false, true, canvas.html());
    
    gl = os.graphics.gl;
    
    //Set Up Control Window
    Win = os.windows.WindowsManager.Create.Window("WebGL GPU AI", "PC");
    Win.Set.position(0,0);
    Win.elements.content.html().style.overflow = "hidden";
    Win.Set.statusbarText("");
    Win.Display.window();
    Win.Set.width(300);
    Win.Set.height(60);
    Win.Set.onMax(os.graphics.OnReset);
    Win.Set.onMin(os.graphics.Pause);
    Win.Hide.menubar();
    Win.elements.content.html().innerHTML = "Press i to toggle: ";
    
    os.graphics.Managers.Camera.MoveUp(100);
    os.graphics.Managers.Camera.MoveLeft(0);
    os.graphics.Managers.Camera.MoveBackward(100);
    
    //Setup Input Controls
    Input = {
        Mouse: {
            lastX: 0,
            lastY: 0,
            pressed: false
        }
    }
    onKeyDown = function(e){
        if(String.fromCharCode(e.keyCode) == "W"){     //Forward
            os.graphics.Managers.Camera.MoveForward(10);   
        }
        else if(String.fromCharCode(e.keyCode) == "S"){     //Backware
            os.graphics.Managers.Camera.MoveBackward(10);
        }
        else if(String.fromCharCode(e.keyCode) == "A"){     //Straif Left
            os.graphics.Managers.Camera.MoveLeft(10);  
        }
        else if(String.fromCharCode(e.keyCode) == "D"){     //Straif Right
            os.graphics.Managers.Camera.MoveRight(10);
        }
        else if(String.fromCharCode(e.keyCode) == "Q"){     //Straif Up
            os.graphics.Managers.Camera.MoveUp(5);
        }
        else if(String.fromCharCode(e.keyCode) == "E"){     //Straif Down
            os.graphics.Managers.Camera.MoveDown(10);
        }
        else if(String.fromCharCode(e.keyCode) == "I"){     //Straif Down
            DrawInfluenceMap();
            //if(gpu){
            //    gpu = false;
            //    Win.elements.content.html().innerHTML = "Press i to toggle: CPU";
            //}
            //else{
            //    gpu = true;
            //    Win.elements.content.html().innerHTML = "Press i to toggle: GPU";
            //}
        }
        else if(String.fromCharCode(e.keyCode) == "C"){     //Straif Down
            
            if(collect){
                collect = false;
            }
            else{
                collect = true;
            }
        }
        
        
    }
    onMouseDown = function(e){
        Input.Mouse.lastX = e.clientX;
        Input.Mouse.lastY = e.clientY;
        Input.Mouse.pressed = true;
    }
    
    onMouseUp = function(e){
        Input.Mouse.pressed = false;
    }
    
    onMouseMove = function(e){
        if (!Input.Mouse.pressed) {
            return;
        }
        var cam = os.graphics.Managers.Camera;
        
        var newX = e.clientX;
        var newY = e.clientY;
    
        var deltaX = newX - Input.Mouse.lastX
        cam.Rotation.yaw += deltaX / 10;
        
        if(cam.Rotation.yaw > 360){ cam.Rotation.yaw  -= 360;}
        else if(cam.Rotation.yaw < 0) { cam.Rotation.yaw += 360; }
    
        var deltaY = newY - Input.Mouse.lastY;
        cam.Rotation.pitch += deltaY / 10;
        if(cam.Rotation.pitch > 360){ cam.Rotation.pitch  -= 360;}
        else if(cam.Rotation.pitch < 0) { cam.Rotation.pitch += 360; }
    
        Input.Mouse.lastX = newX
        Input.Mouse.lastY = newY;   
    }
    
    
    // Setup Event Handlers for User Input
    window.addEventListener("keydown", onKeyDown, false);
    os.graphics.Get.Canvas().addEventListener("mousedown", onMouseDown, false);
    document.addEventListener("mouseup", onMouseUp, false);
    document.addEventListener("mousemove", onMouseMove, false);
}

InfluenceFrameBufferInitialize = function(){
    
    
    
    //Create and bind framebuffer
    influenceFrameBuffer = gl.createFramebuffer();
    
    gl.bindFramebuffer(gl.FRAMEBUFFER, influenceFrameBuffer);
    
    //canvas
    var canvas = os.graphics.Get.Canvas();
    
    influenceFrameBuffer.width = influenceWidth;//canvas.clientWidth;
    influenceFrameBuffer.height = influenceHeight;//canvas.clientHeight;
    
    //Create texture
    os.graphics.Managers.Texture.Create.RenderTexture("influenceRTT", influenceFrameBuffer.width, influenceFrameBuffer.height);
    influenceTexture = os.graphics.Managers.Texture.GetTexture("influenceRTT").texture;
    gl.bindTexture(gl.TEXTURE_2D, influenceTexture);
    
    //Depth Buffer
    var renderbuffer = gl.createRenderbuffer();
    gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
    gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, influenceFrameBuffer.width, influenceFrameBuffer.height);
    
    //Attach texture and depth buffer to framebuffer
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, influenceTexture, 0);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, renderbuffer);
    
    //reset back to defaults
    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    
    //Build CPU Influence Map Texture
    cpuInfluence.canvas = os.resschmgr.Create.HTMLElement("canvas")
    cpuInfluence.width =  influenceWidth;
    cpuInfluence.height = influenceHeight;
    
    cpuInfluence.canvas.html().width = influenceWidth;
    cpuInfluence.canvas.html().height = influenceHeight;
    
    cpuInfluence.ctx = cpuInfluence.canvas.html().getContext('2d');
    
    cpuInfluence.imageData = cpuInfluence.ctx.getImageData(0,0,cpuInfluence.width, cpuInfluence.height);
    
    cpuInfluence.data = []; //cpuInfluence.imageData.data;
    
    //for (var y = 0; y < cpuInfluence.height; ++y) {
    //    for (var x = 0; x < cpuInfluence.width; ++x) {
    //        var index = (y * cpuInfluence.width + x) * 4;
    //
    //        var value = 0xFF;
    //
    //        cpuInfluence.data[index]   = value;    // red
    //        cpuInfluence.data[++index] = value;    // green
    //        cpuInfluence.data[++index] = value;    // blue
    //        cpuInfluence.data[++index] = 255;      // alpha
    //    }
    //}
    
    for (var x = 0; x < cpuInfluence.width; x++) {
        cpuInfluence.data[x] = [128];
        for (var y = 0; y < cpuInfluence.height; y++) {
    
            cpuInfluence.data[x][y] = 0.0;

        }
    }
    
}
DrawInfluenceMap = function(){
    os.graphics.Managers.Camera.CalculateViewMatrix();
    
    gl.bindFramebuffer(gl.FRAMEBUFFER, influenceFrameBuffer);
    
    //Set Clear Color (r,g,b,a)
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    
    //Clear Color and Depth Buffers
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
    //Set Viewport
    os.graphics.gl.viewport(0,0,influenceFrameBuffer.width, influenceFrameBuffer.height);
    //(left, right, bottom, top, near, far, dest)
    //mat4.ortho(-425, 525, 475, 525, -1000, 2000, os.graphics.Matrix.Projection);
    //mat4.ortho(-70, 70, -70, 70, -1, 2000, os.graphics.Matrix.Projection);
    mat4.ortho(-550, 550, -550, 550, -1, 2000, os.graphics.Matrix.Projection);
    //mat4.perspective(45, influenceFrameBuffer.width/influenceFrameBuffer.height, 0.1, 2000.0, os.graphics.Matrix.Projection);

    //Set Camera
    
    //Save current camera position
    var tempPos = os.graphics.Managers.Camera.Position;
    var tempYaw = os.graphics.Managers.Camera.Rotation.yaw;
    var tempPitch = os.graphics.Managers.Camera.Rotation.pitch;
    var tempRoll = os.graphics.Managers.Camera.Rotation.roll;
    
    os.graphics.Managers.Camera.Position = vec3.create([0,10,0]);
    os.graphics.Managers.Camera.Rotation.pitch = 90;
    os.graphics.Managers.Camera.Rotation.roll = 0;
    os.graphics.Managers.Camera.Rotation.yaw = 0;
    
    os.graphics.Managers.Camera.CalculateViewMatrix();
    
    //Draw Objects to Texture
    
    influences.Graphics.Draw();
    
    
    //Reset Framebuffer to default
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    
    //Reset Camera
    os.graphics.Managers.Camera.Position = tempPos;
    os.graphics.Managers.Camera.Rotation.pitch = tempPitch;
    os.graphics.Managers.Camera.Rotation.roll = tempRoll;
    os.graphics.Managers.Camera.Rotation.yaw = tempYaw;
    
    //Set Clear Color (r,g,b,a)
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    
    //Reset Viewport
    os.graphics.OnReset();
}

GPUAI = function(){
    
    //Set Up WebGL and Graphics Core
    GPUAIWebGLInit();
    
    //Set Up Render To Texture for Influence Maps
    InfluenceFrameBufferInitialize();
    
    //Set global Draw and Update Callbacks
    os.graphics.Set.Callback.Draw(GPUAIDraw);
    os.graphics.Set.Callback.Update(GPUAIUpdate);
    
    //Load Textures
    os.graphics.Managers.Texture.Create.Texture("copper", "scripts/Applications/ViRIS/textures/copper.jpg");
    os.graphics.Managers.Texture.Create.Texture("influence", "scripts/Applications/ViRIS/textures/influence.png");
    
    //Draw Buffer
    models =  os.resschmgr.Create.Map();
    
    numToInstance = 350;
    
    //Load Meshes
    
    //Entity Mesh
    var msh = os.graphics.Managers.Mesh.Create.Mesh("sphere", "scripts/Applications/ViRIS/meshes/sphere.json");
    
    //Quad Mesh
    var quad = os.graphics.Managers.Mesh.Create.Primitive.Quad("quad")
    quad.Initialize();
    
    var iQuad = os.graphics.Managers.Mesh.Create.Instanced("quad", "iQuad", numToInstance);
    iQuad.Initialize();
    
    //Set up model properties
    influences = os.graphics.Managers.Entity.Create();
    influences.Graphics.Add.Mesh("iQuad");
    influences.Graphics.Add.Texture("influence");
    influences.Graphics.Set.texture(true);
    influences.Graphics.Set.useBlendColor(true);
    influences.Graphics.Set.blendColor([0.0,0.0,0.0]);
    influences.Graphics.Set.influence(true);
    influences.Set.Scale(1.0,1.0,1.0);
    
    //Set instanced model positions
    for(var i = 0; i < numToInstance; i++){
        //influences.Graphics.Instanced.position[i*3 +  0] = -50.0 + (5.0 * ( i % 20.0) );
        //influences.Graphics.Instanced.position[i*3 +  1] = -45.0;
        //influences.Graphics.Instanced.position[i*3 +  2] = 50.0 - (5.0 * Math.floor(i/20.0));
        
        influences.Graphics.Instanced.position[i*3 +  0] = -500.0 + (50.0 * ( i % 20.0) );
        influences.Graphics.Instanced.position[i*3 +  1] = -45.0;
        influences.Graphics.Instanced.position[i*3 +  2] = 500.0 - (50.0 * Math.floor(i/20.0));
    }
    
    //Set up Influence Map
    influenceMap = os.graphics.Managers.Entity.Create();
    influenceMap.Graphics.Add.Mesh("quad");
    influenceMap.Graphics.Add.Texture("influenceRTT");
    influenceMap.Set.Scale(550.0,550.0,1.0);
    influenceMap.Set.Position(0,13,0); //34
    influenceMap.pitch = 90;
    influenceMap.Graphics.Set.light(false);
    //influenceMap.yaw = 180;
    
    os.graphics.AddToDraw(influenceMap);
    
    //numToInstance = 25;
    msh.onLoad = function(){
        
        //Instance Mesh
        var iSphere = os.graphics.Managers.Mesh.Create.Instanced("sphere", "iSphere", numToInstance);
        iSphere.Initialize();
        
        //Set up model properties
        sphere = os.graphics.Managers.Entity.Create();
        sphere.Graphics.Add.Mesh("iSphere");
        sphere.Graphics.Add.Texture("copper");
        sphere.Graphics.Set.texture(false);
        sphere.Graphics.Set.useBlendColor(true);
        sphere.Graphics.Set.blendColor([0.17,0.26,0.40]);
        sphere.Set.Scale(1.0,1.0,1.0);
        sphere.Set.Position(-500,200,300);
        
        //Set instanced model positions
        for(var i = 0; i < numToInstance; i++){
            sphere.Graphics.Instanced.position[i*3 +  0] = -500.0 + (50.0 * ( i % 20.0) );
            sphere.Graphics.Instanced.position[i*3 +  1] = 15.0;
            sphere.Graphics.Instanced.position[i*3 +  2] = 500.0 - (50.0 * Math.floor(i/20.0));
        }
        
        //Add instanced model to draw buffer
        //os.graphics.AddToDraw(sphere);
        
        //os.graphics.AddToDraw(influences);
        
        
    }
    
    
    //Start Graphics
    os.graphics.Start();    
    
}

GPUAIDraw = function(){
    //timer += os.graphics.Time.dt;
    //
    //if(timer > frequency){
    //    var start = new Date().getTime();
    //   if(gpu){
    //        DrawInfluenceMap();
    //   }
    //   else{ //cpu
    //        //if(sphere)
    //            CPUAI();
    //   }
    //    var end = new Date().getTime();
    //    var diff = end -start;
    //    if(collect){
    //        data.push(diff);
    //        if(data.length > 1000){collect = false;}
    //    }
    //    timer = 0;
    //   
    //}
    sphere.Graphics.Draw();
}

GPUAIUpdate = function(dt){
    Win.Set.statusbarText("FPS: " + (1/dt * 1000).toFixed(3) +", Polys: " + os.graphics.Managers.Mesh.totalPolys + ", Verts: " + os.graphics.Managers.Mesh.totalVerts);

}

CPUAI = function(){
    var charge = 25;
    var falloff = 2;
    
    ClearMap();
    
    for(var i = 0; i < numToInstance; i++){
        CalculateInfluence(-500.0 + (50.0 * ( i % 20.0) ), 500.0 - (50.0 * Math.floor(i/20.0)), charge, falloff);
    }
}

ClearMap = function(){
    
    for (var x = 0; x < cpuInfluence.width; x++) {
        for (var y = 0; y < cpuInfluence.height; y++) {
    
            cpuInfluence.data[x][y] = 0.0;

        }
    }
}
CalculateInfluence = function(x,y,charge, falloff){
    //Convert x and y from World to Map space
    x = Math.ceil(influenceWidth * ((x + 500) / 1000) );
    y = Math.ceil(influenceHeight * ((y + 500) / 1000) );
    
    //Find min and max vlaues for charge position
    var numToFalloff = Math.ceil(charge/falloff);
    
    var minX = x - numToFalloff;
    var maxX = x + numToFalloff
    
    var minY = y - numToFalloff;
    var maxY = y + numToFalloff;
    
    //Clamp min and maxs to stay within map
    if(minX < 0){minX = 0;}
    if(minY < 0){minY = 0;}
    
    if(maxX > influenceWidth){maxX = influenceWidth;}
    if(maxY > influenceHeight){maxY = influenceHeight;}
    
    //Set min and max indicies
    maxX = Math.ceil(maxX);
    minX = Math.ceil(minX);
    
    maxY = Math.ceil(maxY);
    minY = Math.ceil(minY);
    
    //Determine x,y indicies in map
    var row = Math.ceil(x);
    var col = Math.ceil(y);
    
    //Set Influence at position
    cpuInfluence.data[row][col] = charge;
    
    //Fill map influence rings around entity
    var numRings = Math.max(row - minX, col - minY);
    
    for(var i = 0; i < numRings ; i++){
        
        SetInfluenceRing(minX, maxX, minY, maxY, charge);
        charge -= falloff;
        minX++;
        minY++;
        maxX--;
        maxY--;
        
    }
}

SetInfluenceRing = function(rowMin, rowMax, colMin, colMax, value){
    if(rowMin < 0){rowMin = 0;}
    if(colMin < 0){colMin = 0;}
    
    if(rowMax > 127){rowMax = 127;}
    if(colMax > 127){colMax = 127;}
    
    var numInRow = rowMax - rowMin;
    var numInCol = colMax - colMin;
    
    var i = 0;
    while((i < numInRow) && ((colMin + i) < 128)){
        cpuInfluence.data[rowMin][colMin + i] += value;
        cpuInfluence.data[rowMax][colMin + i] += value;
        i++
    }
    
    var j = 0;
    
    while((j < numInCol) && ((rowMin + j) < 128)){
        cpuInfluence.data[rowMin + j][colMin] += value;
        cpuInfluence.data[rowMin + j][colMax] += value;
        
        j++;
    }
}