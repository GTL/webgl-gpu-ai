attribute vec3 aVertexPosition;     //Vertex Position
attribute vec3 aVertexNormal;       //Vertex Normal
attribute vec2 aTextureCoord;       //Texture Coordinate
attribute float aInstance;          //Instance ID

uniform mat4 uWMatrix;              //World
uniform mat4 uVMatrix;              //View
uniform mat4 uPMatrix;              //Projection
uniform mat3 uNMatrix;              //Normal Transform Matrix
uniform vec3 uInstancePosition[500];//Array Holding Positions for each Instanced Object
uniform bool uUseInstacing;         //Enable Instacing

varying vec2 vTextureCoord;         //Output Texture Coordinate
varying vec3 vTransformedNormal;    //Normal Transformed to Clip Space
varying vec4 vPosition;             //Ouput Vert Position in View Space
varying vec3 vPositionW;            //Vert Position in World Space


//Influence Variables
uniform bool uInfluence ;           //Is this an influence charge
uniform vec3 uInfluenceValues[2];   //Holds Influence Values[charge]
varying float vInfluenceID;         //Holds Entity ID

void main(void) {
    vec4 posH;
    
    // View Space Position, used in Lighting Calculations in fragment shader
    vPosition = uVMatrix * uWMatrix * vec4(aVertexPosition, 1.0);
    
    //World Position of Vert
    vPositionW = vec4( uWMatrix * vec4(aVertexPosition, 1.0) ).xyz;
    
    //Output Texture Coord to Fragment Shader
    vTextureCoord = aTextureCoord;
    
    //Output Normal Trnasformed into View Space to Fragment Shader
    //      For Lighting Calculations
    vTransformedNormal = uNMatrix * aVertexNormal;
    
    
    if(!uUseInstacing){
        //Calculates Final Position of Vert
        posH = uPMatrix * uVMatrix * uWMatrix * vec4(aVertexPosition, 1.0);
        //World = uWMatrix;
        
    }
    else{
        
        float offsetX = uInstancePosition[int(aInstance)].x;
        float offsetY = uInstancePosition[int(aInstance)].y;
        float offsetZ = uInstancePosition[int(aInstance)].z;
        
        mat4 WMatrix;
        
        //sin(-90) = -1
        //cos(-90) = 0
        
        vec4 posL = vec4(aVertexPosition, 1.0);
        
        if(uInfluence){
            //Scale Influence based upon charge value
            float charge = float(uInfluenceValues[0].x);
            posL.xyz *= 30.0;
            WMatrix = mat4( 1.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 1.0, 0.0,
                            0.0, -1.0, 0.0, 0.0,
                            offsetX, offsetY, offsetZ, 1.0 );
        }
        else{
        posL.xyz *= 0.1;
            WMatrix = mat4( 1.0, 0.0, 0.0, 0.0,
                            0.0, 1.0, 0.0, 0.0,
                            0.0, 0.0, 1.0, 0.0,
                            offsetX, offsetY, offsetZ, 1.0 );
        }
        
        
        

        
        posH = uPMatrix * uVMatrix * WMatrix * posL;
        
        //Sets Instance ID;
        vInfluenceID = aInstance;
        
    }
    
    gl_Position = posH; 

}